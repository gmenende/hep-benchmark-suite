"""
###############################################################################
# Copyright 2019-2021 CERN. See the COPYRIGHT file at the top-level directory
# of this distribution. For licensing information, see the COPYING file at
# the top-level directory of this distribution.
###############################################################################
"""

__title__        = 'hep-benchmark-suite'
__description__  = 'Benchmark Orchestrator Tool to run several benchmarks.'
__url__          = "https://gitlab.cern.ch/hep-benchmarks/hep-benchmark-suite"
__version__      = '3.0rc1'
__author__       = 'Benchmarking Working Group'
__author_email__ = 'benchmark-suite-wg-devel@cern.ch'
__license__      = 'GPLv3'
