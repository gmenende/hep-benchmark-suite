"""
###############################################################################
# Copyright 2019-2021 CERN. See the COPYRIGHT file at the top-level directory
# of this distribution. For licensing information, see the COPYING file at
# the top-level directory of this distribution.
###############################################################################
"""

import json
import logging
import os
import time

import pkg_resources

from hepbenchmarksuite import benchmarks
from hepbenchmarksuite import db12
from hepbenchmarksuite import utils
from hepbenchmarksuite.exceptions import BenchmarkFailure
from hepbenchmarksuite.exceptions import BenchmarkFullFailure
from hepbenchmarksuite.exceptions import PreFlightError
from hepbenchmarksuite.plugins.construction.config_builder import ConfigPluginBuilder
from hepbenchmarksuite.plugins.construction.dynamic_metadata_provider import DynamicPluginMetadataProvider
from hepbenchmarksuite.plugins.runner import PluginRunner
from hepbenchmarksuite.preflight import Preflight

_log = logging.getLogger(__name__)


class HepBenchmarkSuite:
    """********************************************************
                  *** HEP-BENCHMARK-SUITE ***
     *********************************************************"""
    # Location of result files
    RESULT_FILES = {
        'hs06': 'HS06/hs06_result.json',
        'spec2017': 'SPEC2017/spec2017_result.json',
        'hepscore': 'HEPSCORE/hepscore_result.json',
        'db12': 'db12_result.json',
    }

    def __init__(self, config=None):
        """Initialize setup"""
        self._bench_queue = config['global']['benchmarks'].copy()
        self.selected_benchmarks = config['global']['benchmarks'].copy()
        self._config = config['global']
        self._config_full = config
        self._extra = {}
        self._result = {}
        self.failures = []
        self.preflight = Preflight(config)

        plugin_config = config.get('plugins', {})
        plugin_registry_path = pkg_resources.resource_filename('hepbenchmarksuite.plugins.registry', '')
        self.plugin_metadata_provider = DynamicPluginMetadataProvider(plugin_registry_path)
        plugin_builder = ConfigPluginBuilder(plugin_config, self.plugin_metadata_provider)
        self.plugin_runner = PluginRunner(plugin_builder)

    def start(self):
        """Entrypoint for suite."""
        _log.info("Starting HEP Benchmark Suite")

        self._extra['start_time'] = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime())

        if self.preflight.check():
            _log.info("Pre-flight checks passed successfully.")
            self.plugin_runner.initialize()
            self._run_plugins_synchronously('pre', self._config.get('pre-stage-duration', 0))
            self.run()
            self._extra['end_time'] = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime())
            self._run_plugins_synchronously('post', self._config.get('post-stage-duration', 0))
            self.finalize()
        else:
            _log.error("Pre-flight checks failed.")
            raise PreFlightError

    def _run_plugins_synchronously(self, key, duration_mins: float):
        """Run a plugin synchronously for stage 'key'."""
        _log.debug("Running plugins synchronously '%s' for %.1f minutes.", key, duration_mins)
        self.plugin_runner.start_plugins()

        duration_secs = duration_mins * 60
        time.sleep(duration_secs)

        self.plugin_runner.stop_plugins(key)

    def run(self):
        """Run benchmarks sequentially."""
        for bench2run in self._bench_queue:
            _log.info("Benchmarks left to run: %s", self._bench_queue)
            _log.info("Running benchmark: %s", bench2run)

            self.plugin_runner.start_plugins()
            try:
                return_code = self._run_benchmark(bench2run)
            finally:
                if self.plugin_runner.are_plugins_running():
                    self.plugin_runner.stop_plugins(bench2run)
            _log.info("Completed %s with return code %s", bench2run, return_code)

    def _run_benchmark(self, bench2run):
        if bench2run == 'db12':
            return_code = 0
            result = db12.run_db12(rundir=self._config['rundir'],
                                   cpu_num=self._config_full['global']['ncores'])

            if not result['DB12']['value']:
                self.failures.append(bench2run)
                return_code = 1

        elif bench2run == 'hepscore':
            # Prepare hepscore
            if benchmarks.prep_hepscore(self._config_full) == 0:
                # Run hepscore
                return_code = benchmarks.run_hepscore(self._config_full)
                if return_code < 0:
                    self.failures.append(bench2run)
            else:
                _log.error("Skipping hepscore due to failed installation.")

        elif bench2run in ('hs06', 'spec2017'):
            return_code = benchmarks.run_hepspec(conf=self._config_full, bench=bench2run)
            if return_code > 0:
                self.failures.append(bench2run)

        return return_code

    def finalize(self):
        """Finalize the benchmark execution - collect results, save reports, and check for errors"""
        self._compile_benchmark_results()
        self._save_complete_report()
        self._check_for_workload_errors()

    def _compile_benchmark_results(self):
        self._result = utils.prepare_metadata(self._config_full, self._extra)
        self._result.update({'plugins': self.plugin_runner.get_results()})
        self._result.update({'profiles': {}})

        # Get results from each benchmark
        for bench in self.selected_benchmarks:
            try:
                result_path = os.path.join(self._config['rundir'], self.RESULT_FILES[bench])

                with open(result_path, "r", encoding='utf-8') as result_file:
                    _log.info("Reading result file: %s", result_path)

                    if bench == "hepscore":
                        self._result['profiles']['hepscore'] = json.loads(result_file.read())
                    else:
                        self._result['profiles'].update(json.loads(result_file.read()))

            except Exception as err:  # pylint: disable=broad-except
                _log.warning('Skipping %s because of %s', bench, err)

    def _save_complete_report(self):
        report_file_path = os.path.join(self._config['rundir'], "bmkrun_report.json")
        with open(report_file_path, 'w', encoding='utf-8') as output_file:
            dump = json.dumps(self._result)
            _log.info("Saving final report: %s", report_file_path)
            _log.debug("Report: %s", dump)
            output_file.write(dump)

    def _check_for_workload_errors(self):
        if len(self.failures) == len(self.selected_benchmarks):
            _log.error('All benchmarks failed!')
            raise BenchmarkFullFailure
        if len(self.failures) > 0:
            _log.error("%s Failed. Please check the logs.", self.failures)
            raise BenchmarkFailure
        _log.info("Successfully completed all requested benchmarks")
